import { Router } from "express";
const addressRouter = Router();
import { ListProvinces, ListDistrict, ListWard } from "../controllers/address";

addressRouter.get("/provinces", ListProvinces);
addressRouter.get("/districts/:provinceCode", ListDistrict);
addressRouter.get("/wards/:districtCode", ListWard);

export default addressRouter;
