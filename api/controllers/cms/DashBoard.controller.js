import { AxiosConnectBackend } from "../../axios";

export const getNumberOfRegistrationRecent7Days = async (req, res) => {
  const token = req.headers.authorization;
  try {
    const response = await AxiosConnectBackend.get(`/cms/recent7days`, {
      headers: {
        authorization: `Bearer ${token}`,
      },
    });
    res.json(response.data);
  } catch (error) {
    res.json([]);
    console.log(error);
  }
};
export const getNumberOfRegistrationCities = async (req, res) => {
  const token = req.headers.authorization;
  try {
    const response = await AxiosConnectBackend.get(`/cms/cities`, {
      headers: {
        authorization: `Bearer ${token}`,
      },
    });
    res.json(response.data);
  } catch (error) {
    res.json([]);
    console.log(error);
  }
};
