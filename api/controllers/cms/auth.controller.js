import { AxiosConnectBackend } from "../../axios";

export const refreshToken = async (req, res) => {
  const token = req.headers.authorization;
  try {
    const { data } = await AxiosConnectBackend.get("/auth/generate-token", {
      headers: {
        authorization: `Bearer ${token}`,
      },
    });
    res.json(data);
  } catch (error) {
    console.log("refreshToken auth.controller.js");
    console.log(error);
    res.status(500).end();
  }
};
