import { AxiosExpress } from "../api/axios";
import { mappingFormData } from "../utils/mapping";
import { message } from "ant-design-vue";
import { deleteCookie, getCookie, setCookie } from "../utils/cookie";
import { checkExpireToken } from "../utils/auth";

export const state = () => ({
  currentStep: 0,
  formMode: null,
  formData: {},
  listProvinces: [],
  listDistrict: [],
  listWard: [],
  adminInfo: {
    username: "",
    token: "",
  },
  formToken: "",
  listRole: [],
setting:{
title:"Tiếp sức đến trường | Tuổi trẻ online",
enable:true,
}
});

export const mutations = {
  SetStep: (state, payload) => {
    state.currentStep = payload;
  },
  SetFormData: (state, payload) => {
    state.formData = payload;
  },
  SetFormMode: (state, payload) => {
    state.formMode = payload;
  },
  ResetState: (state) => {
    state.currentStep = 0;
    state.formMode = null;
    state.formData = {};
  },
  LIST_PROVINCES: (state, payload) => {
    state.listProvinces = payload;
  },
  LIST_DISTRICTS: (state, payload) => {
    state.listDistrict = payload;
  },
  LIST_WARDS: (state, payload) => {
    state.listWard = payload;
  },
  ClearAdminInfo: (state) => {
    state.adminInfo = {
      username: "",
      token: "",
    };
  },
  SetAdminInfo: (state, payload) => {
    state.adminInfo = payload;
  },
  SetFormToken: (state, payload) => {
    state.formToken = payload;
    setCookie("formToken", payload);
  },
  ClearFormToken: (state) => {
    state.formToken = "";
  },
  SetListRole: (state, payload) => {
    state.listRole = payload;
  },
  ClearListRole: (state) => {
    state.listRole = [];
  },
SetSetting:(state, payload) => {
state.setting = payload
}
};

export const actions = {
  async nuxtServerInit({ commit }) {
    const token = this.$cookies.get("_ttoauth");
    if (!token || !checkExpireToken(token)) return;
    try {
      this.$axios.setToken(token);
      const res = await this.$axios.get("/api/cms/getListRole");
const settingRes = await this.$axios.get('/api/cms/getSetting')
const {title,status}=settingRes.data
commit('SetSetting',{title, enable:Boolean(status)})
      commit("SetListRole", res.data);
console.log(settingRes.data)
      console.log(res.data);
    } catch (error) {
       console.log("function nuxtServerInit store", error);
    }
  },
  initFormData({ rootState, commit, dispatch, redirect }) {
    dispatch("GetProvinces");
    dispatch("loadFormToken");
  },
  loadFormToken({ commit, dispatch }) {
    const token = this.$cookies.get("formToken");
    if (token && checkExpireToken(token)) {
      commit("SetFormToken", token);
    } else {
      deleteCookie("formToken");
      dispatch("refreshToken", token);
    }
  },
  async refreshToken({ commit }, tokenExpire) {
    try {
      this.$axios.setToken(tokenExpire, "Bearer");
      const res = await this.$axios.get("/api/cms/getNewToken");
      commit("SetFormToken", res.data);
    } catch (error) {
      console.log("refreshToken ./store/index.js");
      message.error(
        "Đã có lỗi xảy ra, vui lòng tải lại trang, nếu tình trạng vẫn xảy ra, hãy liên hệ hotline ngay lập tức"
      );
    }
  },
  async GetProvinces({ commit, dispatch }) {
    commit("LIST_DISTRICTS", []);
    try {
      const res = await AxiosExpress.get("/api/address/provinces");
      const { provinces } = res.data;
      commit("LIST_PROVINCES", provinces);
    } catch ({ response }) {
      console.log("GetProvinces ./store/index.js");
      console.log(response.status);
      console.log(response.statusText);
    }
  },
  async GetDistricts({ rootState, commit, dispatch, redirect }, provinceCode) {
    commit("LIST_WARDS", []);
    try {
      const res = await AxiosExpress.get(
        `/api/address/districts/${provinceCode}`
      );
      const { districts } = res.data;
      commit("LIST_DISTRICTS", districts);
    } catch (error) {
      console.log("GetDistricts ./store/index.js", error);
    }
  },

  async GetWards({ rootState, commit, dispatch, redirect }, districtCode) {
    try {
      const res = await AxiosExpress.get(`/api/address/wards/${districtCode}`);
      const { wards } = res.data;
      commit("LIST_WARDS", wards);
    } catch (error) {
      console.log("GetWards ./store/index.js", error);
    }
  },

  async createNewRequest({ commit, state }, { formData, formMode }) {
    console.log(state);
    const submitData = mappingFormData(formMode, formData);
    try {
      const res = await AxiosExpress.post(
        `/api/schoolarship/create-schoolarship-request/${formMode}`,
        submitData,
        {
          headers: {
            authorization: state.formToken,
          },
        }
      );
      const { message, status } = res;
      if (status === 200) {
        this.$router.push("/finish");
        window.finishForm = true;
      } else {
        message.error(`${message}`);
      }
    } catch (error) {
      console.log(error);
      message.error(error.response.data.message);
    }
  },

  logout({ commit }) {
    commit("ClearAdminInfo");
    commit("ClearListRole");
  },
  getAdminInfo({ commit }, adminInfo) {
    commit("SetAdminInfo", adminInfo);
  },
};
