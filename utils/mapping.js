import { checkEmptyObject } from './common'
export const mappingFormData = (formMode, formData) => {
    let mapping = {}
    if (formMode === 'tan-sinh-vien') {
        mapping = {
            frontCMND:
                formData.matTruocCMND[formData.matTruocCMND?.length - 1]
                    ?.originFileObj,
            backCMND:
                formData.matSauCMND[formData.matSauCMND?.length - 1]
                    ?.originFileObj,
            certification:
                formData.giayXacNhan[formData.giayXacNhan?.length - 1]
                    ?.originFileObj,
            admissionNotice:
                formData.giayBao[formData.giayBao?.length - 1]?.originFileObj,
            // -------------------------------------------
            name: formData.ten,
            surname: formData.ho,
            sex: formData.gioiTinh,
            dateOfBirth: formData.ngayThangNamSinh,
            address: formData.diaChi,

            city: formData.tinhThanhPho,
            district: formData.quanHuyen,
            village: formData.phuongXa,
            email: formData.email,
            phone: formData.soDienThoai,
            highSchoolName: formData.tenTruongCap3,
            score10: formData.diemLop10,
            score11: formData.diemLop11,
            score12: formData.diemLop12,
            scoreSubject1: formData.diemMon1,
            scoreSubject2: formData.diemMon2,
            scoreSubject3: formData.diemMon3,
            total: formData.tongDiem,
            academicAchievement: formData.thanhTichHocTap,
            scholarship: formData.hocBong,
            major: formData.nganhHocDaiHoc,
            university: formData.truongDaiHoc,
            matriculation: formData.hinhThuc,
            note: formData.ghiChu,
            numberCMND: formData.soCMND,
            date: formData.ngayCap,
            issued_by: formData.noiCap,
            introduce: formData.gioiThieuBanThan,
            activity: formData.cacHoatDong,
            overcomeChallenge: formData.vuotQuaThachThuc,
            futureOrientation: formData.dinhHuongTuongLai,
            whyNeed: formData.taiSaoDuocHocBong,
            question: formData.cauHoi,
            introduceFamily: formData.gioiThieuGiaDinh,
            reason: formData.lyDoCanHocBong,
            reciprocate: formData.dapDenTiepNoi,
            moreSupportScholarship: formData.hoTroNgoaiHocBong,
            moreInformation: formData.boSungThongTin,
        }
    } else {
        mapping = {
            frontCMND:
                formData.matTruocCMND[formData.matTruocCMND?.length - 1]
                    ?.originFileObj,
            backCMND:
                formData.matSauCMND[formData.matSauCMND?.length - 1]
                    ?.originFileObj,
            admissionNotice:
                formData.giayBao[formData.giayBao?.length - 1]?.originFileObj,
            // -------------------------------------------
            namePresenter: formData.tenNguoiGioiThieu,
            surname: formData.ho,
            address: formData.diaChi,
            city: formData.tinhThanhPho,
            email: formData.email,
            phone: formData.soDienThoai,
            job: formData.ngheNghiep,
            relationship: formData.moiQuanHe,
            note: formData.ghiChu,

            namePresentee: formData.tenSinhVien,
            surnamePresentee: formData.hoSinhVien,
            addressPresentee: formData.diaChiSinhVien,
            cityPresentee: formData.tinhThanhPhoSinhVien,
            districtPresentee: formData.quanHuyenSinhVien,
            villagePresentee: formData.phuongXaSinhVien,
            emailPresentee: formData.emailSinhVien,
            phonePresentee: formData.soDienThoaiSinhVien,
            major: formData.nganhHocDaiHoc,
            university: formData.truongDaiHoc,
            method: formData.phuongThucTrungTuyen,
            situation: formData.hoanCanhGiaDinh,
            effort: formData.noLucCaNhan,
            description: formData.thongTinKhac,
        }
    }
    const form = new FormData()
    for (let key in mapping) {
        form.append(key, mapping[key])
    }
    return form
}

const pattern = {
    'cms/get-student': 'Xem danh sách sinh viên đăng ký',
    'cms/get-presenter': 'Xem danh sách người giới thiệu đăng ký',
    'cms/scholarship-detail': 'Xem chi tiết thông tin sinh viên đăng ký',
    'cms/scholarship-details':
        'Xem chi tiết thông tin người giới thiệu đăng ký',
    'cms/export-student': 'Xuất excel danh sách sinh viên',
    'cms/export-presenter': 'Xuất excel danh sách người giới thiệu',
    'cms/recent7days': 'Biểu đồ lượt đăng ký trong tuần',
    'cms/cities': 'Biểu đồ  lượt đăng ký theo tỉnh thành',
    'cms/update-status': 'Chỉnh sửa cài đặt',
}
export const mappingRoles = roles =>
    roles.map(action_name => ({
        label: pattern[action_name],
        key: action_name,
    }))

export const generateURL = (baseURL, params = {}) => {
    if (!params || checkEmptyObject(params)) return baseURL
    let url = baseURL
    const keys = Object.keys(params)
    keys.forEach(key => {
        if (params[key]) {
            url += `${url[url.length - 1] === '?' ? '' : '&'}${key}=${
                params[key]
            }`
        }
    })
    return url
}
